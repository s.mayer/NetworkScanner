
package nl.test.networkscanner;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class PingIPAddress {

    private IpV4Converter ipV4Converter;
    private long ipAddress;

    public PingIPAddress(IpV4Converter ipV4Converter) {
        this.ipV4Converter = new IpV4Converter();        
    }

    public List<String> pingAdres(String minIpAdress, String maxIpAdress) {
        long longMinIpAdress = ipV4Converter.StringToIntIP(minIpAdress);
        long longMaxIpAdress = ipV4Converter.StringToIntIP(maxIpAdress);
        InetAddress addr = null;
        List<String> reachedIpAdress = new ArrayList<>();
        ipAddress = longMinIpAdress;
        for (; ipAddress <= longMaxIpAdress;) {
            long reachable = ping(ipV4Converter.longToStringIP((int) ipAddress));
            if (reachable == 1) {             
                InetAddress addr1 = null;
                try {
                    addr1 = InetAddress.getByName(ipV4Converter.longToStringIP(ipAddress));
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String host = addr1.getHostName();
                System.out.println(host);
                 
                String test = host+"/"+ipV4Converter.longToStringIP(ipAddress);
                
                reachedIpAdress.add(test);
            }
            ipAddress++;
        }
        return reachedIpAdress;
    }  
     
    /**
     * @param ipAddress
     */
    private static int ping(String ipAddress) {
        try {
            InetAddress inet = InetAddress.getByName(ipAddress);
            System.out.println("Sending Ping Request to " + ipAddress);
            if (inet.isReachable(500)) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            System.out.println("Exception:" + e.getMessage());
        }
        return -1;
    }
}