package nl.test.networkscanner;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 
 */
public class PortScanner {

    private final ExecutorService executorService;
    private final List<String> reachableIpadress;
    private static final int TIMEOUT = 200;

    public PortScanner(ExecutorService executorService, List<String> reachableIpadress) {
        this.executorService = executorService;
        this.reachableIpadress = reachableIpadress;
    }

    public void executePostScan() {
        
        for (String ipAdress : reachableIpadress) {
            final List<Future<ScanResult>> futures = new ArrayList<>();
            for (int port = 1; port <= 10000; port++) {
                // for (int port = 1; port <= 80; port++) {
                futures.add(portIsOpen(executorService, ipAdress, port, TIMEOUT));
            }
            try {
                executorService.awaitTermination(999L, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            int openPorts = 0;
            for (final Future<ScanResult> f : futures) {
                try {
                    if (f.get().isOpen()) {
                        openPorts++;
                        System.out.println("Port "+f.get().getPort());
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("There are " + openPorts + " open ports on host " + ipAdress +
                    " (probed with a timeout of " + TIMEOUT + "ms)");

        }
    }
 

    public static Future<ScanResult> portIsOpen(final ExecutorService es, final String ip, final int port,
            final int timeout) {
        return es.submit(new Callable<ScanResult>() {
            @Override
            public ScanResult call() {
                try {
                    Socket socket = new Socket();
                    socket.connect(new InetSocketAddress(ip, port), timeout);
                    socket.close();
                    return new ScanResult(port, true);
                } catch (Exception ex) {
                    return new ScanResult(port, false);
                }
            }
        });
    }
}