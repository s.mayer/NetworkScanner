package nl.test.networkscanner;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 */
public class NetworkScanner {
    
    private static final String MIN_IP_ADRESS = "192.168.13.1";
    private static final String MAX_IP_ADRESS = "192.168.14.1";
    
    public static void main(String[] args) {
        final ExecutorService executorService = Executors.newFixedThreadPool(250);
        IpV4Converter ipV4Converter = new IpV4Converter();
        PingIPAddress pingIPAddress = new PingIPAddress(ipV4Converter);
        
        List<String> reachableIpadress = pingIPAddress.pingAdres(MIN_IP_ADRESS, MAX_IP_ADRESS);
        System.out.println("reachableIpadress "+ reachableIpadress.size());
        PortScanner portScanner = new PortScanner(executorService, reachableIpadress);
        portScanner.executePostScan();
        
    }
        
}
 
